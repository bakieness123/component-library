# TISC Component Library

![Selector][logo]

[logo]: https://bitbucket.org/bakieness123/component-library/raw/7b9acc230404b492ae7508870499e10b48d38b57/background%403x.png "Logo Title Text 2"

## Install
Install the npm package by running the following code.
```
npm install @mrbakieness/tisc_nuxt --save-dev
```

## Usage
For the components to be displayed correctly import the css in the nuxt.config.js file with the following code.
```javascript
css: [
    '@mrbakieness/tisc_nuxt/tisc.min.css'
  ]
```
Once installed you can import any component from the library using the following code on your vue file.
```javascript
import { tiscButton } from 'tisc_nuxt';


components: { tiscButton },
```
Then you can use the component in your template like the following.
```html
<tisc-button color='yellow' outline>button</tisc-button>
```